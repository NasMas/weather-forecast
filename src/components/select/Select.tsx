import React, {Dispatch, SetStateAction, useState} from 'react';
import s from './select.module.css';

type SelectType = {
    styles: { readonly [key: string]: string; },
    placeholder: string,
    options: (string | number)[][],
    isCityPiked: boolean,
    setIsCityPiked: Dispatch<SetStateAction<boolean>>,
    setCoordinates: (latitude: number, longitude: number) => void,
}

export const Select = (props: SelectType) => {

    const [pikedCity, setPikedCity] = useState(props.placeholder);

    const Options = () => {
        const options = props.options.map((option: any) =>
            <li key={option[0]} onClick={() => {
                console.log(option[0] + ' was clicked' + option[1] + option[2]);
                setPikedCity(option[0]);
                props.setIsCityPiked(true);
                props.setCoordinates(option[1], option[2]);
            }}>{option[0]}</li>
        );
        return <ul className={props.styles.shift_options}>{options}</ul>;
    }

    return (
        <details className={props.styles.shift}>
            <summary className={props.isCityPiked ? s.piked : s.unpicked}>
                {pikedCity}
            </summary>
            <Options/>
        </details>
    )
}