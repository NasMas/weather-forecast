import React, {useEffect, useState} from 'react';
import s from "./seven_days_weather_forecast.module.css";
import cloud from "../../icons/Academy-Weather-bg160.svg";
import {Select} from "../select/Select";
import ArrowLeft from "../../icons/arrow-left.svg";
import ArrowRight from "../../icons/arrow-right.svg";
import {months} from "../../constants/months";
import {cities} from "../../constants/cities";

export const SevenDaysWeatherForecast = () => {

    const [isCityPiked, setIsCityPiked] = useState(false);
    const [sevenDaysWeatherForecast, setSevenDaysWeatherForecast] = useState([[]]);
    const [firstVisibleCard, setFirstVisibleCard] = useState(0);
    const [lastVisibleCard, setLastVisibleCard] = useState(2);
    const windowSize = useWindowSize();

    const getSevenDaysWeatherForecast = (latitude: number, longitude: number) => {
        fetch('https://api.openweathermap.org/data/2.5/onecall?lat=' + latitude + '&lon=' + longitude + '&appid=aafdb5f809e160cf1aea4d8d0e4107bf', {
            method: 'GET',
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                let dataArray: any = [];
                [1, 2, 3, 4, 5, 6, 7].forEach((i) => {
                        const date = new Date(data.daily[i].dt * 1000);
                        dataArray.push([
                            date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear(),
                            "http://openweathermap.org/img/wn/" + data.daily[i].weather[0].icon + "@2x.png",
                            (Math.round(data.daily[i].temp.day - 273.15) > 0 ? '+' + Math.round(data.daily[i].temp.day - 273.15) + '°' : '-' + Math.round(data.daily[i].temp.day - 273.15) + '°')])
                    }
                );
                setSevenDaysWeatherForecast(dataArray);
            });
    }

    const Cards = () => {
        const cards: any = [];
        if (windowSize.width <= 660) {
            [0, 1, 2, 3, 4, 5, 6].forEach((i) => {
                    if (sevenDaysWeatherForecast[i] !== undefined) {
                        cards.push(<div className={s.card}>
                            <p className={s.date}>{sevenDaysWeatherForecast[i][0]}</p>
                            <img className={s.weather_icon} src={sevenDaysWeatherForecast[i][1]} alt="weather_icon"/>
                            <p className={s.temp}>{sevenDaysWeatherForecast[i][2]}</p>
                        </div>)
                    }
                }
            );
        } else {
            [firstVisibleCard, lastVisibleCard - 1, lastVisibleCard].forEach((i) => {
                    if (sevenDaysWeatherForecast[i] !== undefined) {
                        cards.push(<div className={s.card}>
                            <p className={s.date}>{sevenDaysWeatherForecast[i][0]}</p>
                            <img className={s.weather_icon} src={sevenDaysWeatherForecast[i][1]} alt="weather_icon"/>
                            <p className={s.temp}>{sevenDaysWeatherForecast[i][2]}</p>
                        </div>)
                    }
                }
            );
        }
        return (
            <div className={s.cards_layout}>
                <img alt="arrow_left" className={(firstVisibleCard === 0) ? s.arrow_left_dis : s.arrow_left} src={ArrowLeft} onClick={() => {
                    setFirstVisibleCard(firstVisibleCard - 1);
                    setLastVisibleCard(lastVisibleCard - 1);
                }}/>
                {cards}
                <img alt="arrow_right" className={(lastVisibleCard === 6) ? s.arrow_right_dis : s.arrow_right} src={ArrowRight} onClick={() => {
                    setFirstVisibleCard(firstVisibleCard + 1);
                    setLastVisibleCard(lastVisibleCard + 1);
                }}/>
            </div>
        );
    }

    const Content = () => {
        if (!isCityPiked) {
            return (
                <div>
                    <img alt="cloud" className={s.cloud} src={cloud}/>
                    <p className={s.error_msg}>Fill in all the fields and the weather will be displayed</p>
                </div>
            )
        } else return (
            <Cards/>
        )
    }

    return (
        <div className={s.background}>
            <p className={s.title}>7 Days Forecast</p>
            <Select styles={s} placeholder="Select city" options={cities} isCityPiked={isCityPiked}
                    setIsCityPiked={setIsCityPiked} setCoordinates={getSevenDaysWeatherForecast}/>
            <Content/>
        </div>
    )
}

function useWindowSize() {
    // Initialize state with undefined width/height so server and client renders match
    // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
    const [windowSize, setWindowSize] = useState({
        width: 0,
        height: 0,
    });
    useEffect(() => {
        // Handler to call on window resize
        function handleResize() {
            // Set window width/height to state
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }

        // Add event listener
        window.addEventListener("resize", handleResize);
        // Call handler right away so state gets updated with initial window size
        handleResize();
        // Remove event listener on cleanup
        return () => window.removeEventListener("resize", handleResize);
    }, []); // Empty array ensures that effect is only run on mount
    return windowSize;
}