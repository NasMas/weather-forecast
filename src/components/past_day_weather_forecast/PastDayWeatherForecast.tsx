import React, {useState} from 'react';
import s from "./past_day_weather_forecast.module.css";
import {Datepicker} from "../datepicker/Datepicker";
import cloud from "../../icons/Academy-Weather-bg160.svg";
import {Select} from "../select/Select";
import {months} from "../../constants/months";
import {cities} from "../../constants/cities";

export const PastDayWeatherForecast = () => {

    const [isCityPiked, setIsCityPiked] = useState(false);
    const [isDatePicked, setIsDatePicked] = useState(false);
    const [date, setDate] = useState('');
    const [pastDayWeatherForecast, setPastDayWeatherForecast] = useState(["","",""]);
    const [latitude, setLatitude] = useState(0);
    const [longitude, setLongitude] = useState(0);
    const [isRequestDone, setIsRequestDone] = useState(false);

    const setCoordinates = (latitude: number, longitude: number) => {
        setLatitude(latitude);
        setLongitude(longitude);
        setIsRequestDone(false);
    }

    const Card = () => {
        return (
            <div className={s.cards_layout}>
                <div className={s.card}>
                    <p className={s.date}>{pastDayWeatherForecast[0][0]}</p>
                    <img className={s.weather_icon} src={pastDayWeatherForecast[0][1]} alt="weather_icon"/>
                    <p className={s.temp}>{pastDayWeatherForecast[0][2]}</p>
                </div>
            </div>
        );
    }

    const Content = () => {
        if (!isCityPiked || !isDatePicked) {
            return (
                <div>
                    <img className={s.cloud} src={cloud} alt="cloud"/>
                    <p className={s.error_msg}>Fill in all the fields and the weather will be displayed</p>
                </div>
            )
        } else {
            if(!isRequestDone) {
                fetch('https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=' + latitude + '&lon=' + longitude + '&dt=' + new Date(date).getTime() / 1000 + '&appid=aafdb5f809e160cf1aea4d8d0e4107bf', {
                    method: 'GET',
                })
                    .then(function (response) {
                        return response.json();
                    })
                    .then(function (data) {
                        let dataArray: any = [];
                        const date = new Date(data.current.dt * 1000);
                        const weatherIcon = data.current.weather[0].icon.replace('n','d');
                        dataArray.push([
                            date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear(),
                            "http://openweathermap.org/img/wn/" + weatherIcon + "@2x.png",
                            (Math.round(data.current.temp - 273.15) > 0 ? '+' + Math.round(data.current.temp - 273.15) + '°' : '-' + Math.round(data.current.temp - 273.15) + '°')]);
                        setPastDayWeatherForecast(dataArray);
                        setIsRequestDone(true)
                    });
            }
            return (
                <Card />
            )
        }

    }

    return (
        <div className={s.background}>
            <p className={s.title}>Forecast for a Date in the Past</p>
            <div className={s.inputs_layout}>
                <Select styles={s} placeholder="Select city" options={cities} isCityPiked={isCityPiked}
                        setIsCityPiked={setIsCityPiked} setCoordinates={setCoordinates}/>
                <Datepicker placeholder="Select date" styles={s} isDatePicked={isDatePicked}
                            setIsDatePicked={setIsDatePicked}
                            setDate={setDate} setIsRequestDone={setIsRequestDone}/>
            </div>
            <Content/>
        </div>
    )
}