type TupleToObject<A extends ReadonlyArray<string | number>, T> = {
    [P in Exclude<keyof A, keyof ReadonlyArray<string | number>> as Extract<A[P], string | number>]: T;
};

export {};