type StringFormat<S extends string>
    = S extends `${infer BeforeS}%s${infer AfterS}`
    ? BeforeS extends `${infer _}%d${infer _}`
        ? S extends `${infer _}%d${infer AfterD}`
            ? (d: number) => StringFormat<AfterD>
            : (s: string) => StringFormat<AfterS>
        : (s: string) => StringFormat<AfterS>
    : S extends `${infer _}%d${infer AfterD}`
        ? (d: number) => StringFormat<AfterD>
        : string;

export {};