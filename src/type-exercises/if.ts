type If<A1 extends boolean, A2, A3> = A1 extends true ? A2 : A3;

export {};