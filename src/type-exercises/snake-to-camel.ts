type SnakeToCamel<S extends string>
    = S extends `${infer _} ${infer _}`
    ? never
    : S extends `${infer Before}_${infer After}`
        ? `${Lowercase<Before>}${Capitalize<SnakeToCamel<After>>}`
        : `${Lowercase<S>}`;

export {};