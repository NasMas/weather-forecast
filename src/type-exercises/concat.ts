type Concat<A1 extends unknown[], A2 extends unknown[]> = [...A1, ...A2];

export {};