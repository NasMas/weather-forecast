type StringReplace<BaseS extends string, Regexp extends string, Substr extends string>
    = BaseS extends `${infer Before}${Regexp}${infer After}`
    ? `${Before}${Substr}${StringReplace<After, Regexp, Substr>}`
    : BaseS extends `${infer Before}${Capitalize<Regexp>}${infer After}`
        ? `${Before}${Capitalize<Substr>}${StringReplace<After, Regexp, Substr>}`
        : BaseS extends `${infer Before}${Uppercase<Regexp>}${infer After}`
            ? `${Before}${Uppercase<Substr>}${StringReplace<After, Regexp, Substr>}`
            : BaseS;

export {};