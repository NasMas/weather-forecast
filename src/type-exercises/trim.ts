type Trim<S extends string> = S extends `${infer N} ` | ` ${infer N}` ? Trim<N> : S;

export {};