function merge<T,V>(object1: T, object2: V): T&V {
    return {...object1, ...object2}
}
export {}