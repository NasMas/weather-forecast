/**
 * @function
 * @template T
 * @param{boolean} predicate
 * @param{T[]} collection
 * @returns {T[]}
 */
function filter(predicate, collection) {
    return collection.reduce((acc, value) => {
        if (predicate(value)) return [...acc, value];
        return [...acc]
    }, []);
}