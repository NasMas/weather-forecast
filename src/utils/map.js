/**
 * @function
 * @template T
 * @param {T[]} collection
 * @template V
 * @param {V} callback
 * @returns {V[]}
 */
function map(callback, collection) {
    return collection.reduce((acc, value) => [...acc, callback(value)], []);
}