type PaddingValue = `${number} px`;
type Padding = {
    top: PaddingValue,
    right: PaddingValue,
    bottom: PaddingValue,
    left: PaddingValue
}

function padding(): Padding;
function padding(padding: number): Padding;
function padding(paddingHorizontal: number, paddingVertical: number): Padding;
function padding(paddingTop: number, paddingHorizontal: number, paddingBottom: number): Padding;
function padding(paddingTop: number, paddingRight: number, paddingBottom: number, paddingLeft: number): Padding;

function padding(a?: number, b?: number, c?: number, d?: number): Padding{
    if (a) {
        if (b) {
            if (c) {
                if (d) {
                    return {top: `${a} px`, right: `${b} px`, bottom: `${c} px`, left: `${d} px`}
                } else return {top: `${a} px`, right: `${b} px`, bottom: `${c} px`, left: `${b} px`}
            } else return {top: `${a} px`, right: `${b} px`, bottom: `${a} px`, left: `${b} px`}
        } else return {top: `${a} px`, right: `${a} px`, bottom: `${a} px`, left: `${a} px`}
    } else return {top: '0 px', right: '0 px', bottom: '0 px', left: '0 px'};
}

export {}